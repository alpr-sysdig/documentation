Doc for instruction.mj :

+ This is the main file (maybe should be renamed or split).

+ We aim at keeping the organization close to this diagram :
http://ac.aua.am/Arm/Public/2017-Spring-Computer-Organization/Textbooks/ComputerOrganizationAndDesign5thEdition2014.pdf#page=288&zoom=page-width,22,621

However we will have to modify it as it is for MIPS

Modifications :
- We suppressed the AluOP wire as ALU control can handle everything itself


Organisation:

the function cycle assemble all the necessary functions to accomplish one cycle of computation. This starts with fetch wich retrives the next instruction from memory which is then split by decode.
Then controle interprets this instrcution.
register takes care of access to registers


Controle :
This will be a tricky point. We can't use if so we will have to express each result in form of a boolean formula.
Here's what I propose so we add functionnalities more easily without having to redo this function from scratch everytime :
we can separate instructions in 7 families that have similar roles and opcodes :
 - arithmetic : std and immeditae
 - Loads
 - Store
 - Call
 - Branch
 - Jump 
 
 we had started a "descision tree" : this may be useful to you : 
if opcode[2] = 1 then
        (*JAL and JALR*)
     else 
        if opcode[6] = 1 then
            (*Call and branch *)
        else
            if opcode[4] = 1 then
                if opcode[5] = 1 then
                    (*arithmetic std*)
                 else
                    (*arithmetic immediate*)

             end if
            else 
              (*load and store*)
     end if
     end if
     end if
     
now the principe is that when you are treating a certain type of instruction, you first compute the boolean value of your return parameter only for this class of instruction and then we do a "or" on all these values. See instruction.mj for an example

For jump :
we use th ALU to add rs1 to imm -> so we set : ALUSrc = 0, abd we take back aluresult


ALU :

The ALU returns the result ALU result and a flag for comparison : 1 = true, 0 = false

 
