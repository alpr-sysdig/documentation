# Specifications
## Loads
+ LW

## Store
+ SW

## Shift
+ SLL
+ SLLI
+ SRL
+ SRLI
+ SRA
+ SRAI

## Arithmetic
+ ADD
+ ADDI
+ SUB

## Logical
+ XOR
+ XORI
+ OR
+ ORI
+ AND
+ ANDI

## Compare
+ SLT
+ SLTI
+ SLTU
+ SLTIU


## Jump
+ JAL
+ JALR

## Branch
+ BEQ
+ BNE
+ BLT
+ BGE
+ BLTU
+ BGEU

## System
+ CALL

## Multiply-Divide
+ MUL
+ DIV
+ DIVU
+ REM
+ REMU

