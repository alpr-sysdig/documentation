# Style Guide #

## Source code layout ##
- Do not use tabs, use 2 spaces for each indentation level
- Do not leave whitespace at the end of a line
- Do not write anything past column 80 :
  If your line is more than 80 characters long, split it.

## Syntax ##
- Do not use spaces after (, [ of before ), ], !, ?, :, ;

## Naming ##
+ TODO: Naming variables
+ TODO: CamelCase vs snake_case

## Comments ##
- Comment your code when needed
- Do not comment functions when their output is obvious
- Do not leave a complicated function uncommented

## Misc ##
- Functions should be short. Split complicated functions
- If the code you wrote generated warnings, use the WARNING keyword
- If the code you wrote has known bugs, use the FIXME keyword
- If something has to be done next but you don't have time, use te TODO keyword

## Wrapping ##
- If functions need long documentation, add it to the README
- All compilation and testing commands should be done through make

## Testing ##
+ TODO: Unit Testing
+ TODO: Debugging

## Distributing ##

- Write proper commit messages. Not only is it easier to read, it is also a
  whole lot faster to debug. The last thing you want to do is lose time debugging.

    The rule is: every good commit should be able to complete the following sentence
    "**When applied, this commit will: {{ YOUR COMMIT MESSAGE}}**"

        – When applied this commit will *Update README file*
        – When applied this commit will *Add validation for GET /user/:id API call*
        – When applied this commit will *Revert commit 12345*

    For more information, read [Chris Beam's guide](https://chris.beams.io/posts/git-commit/)

- Do not mess with "master" branch.
  When offline, work on a local branch, for instance "yourname".
  When you have committed to "yourname" and are ready to push, use
  `git rebase master` and then `git push origin master`.
  This will lead to linear history, which will allow easier debugging

- Do not include temporary files, or editor-specific files to a commit.
  That is to say, all files ending with "~" or ".swp", or directories like
  "\_build", or ".native" files, etc.
  Put those files in you .gitignore if you don't want them shown in `git status`

- Use `git status` and `git diff` and `git diff --staged`. A lot.
  You should always know what happens in your commit, to avoid useless splits
  and ensure all your work is pushed

- Use TODO, FIXME and WARNING keywords (followed by ":"), so others know
  what has to be done.  By the way, this subsection was first a TODO.
  Use `git checkout <commit hash>` to have a look at it.

- Look for the previous keywords left by others, see if you can help.
  Use `grep KEYWORD: -r .` to see which files contain it (don't forget ":")

## Exceptions ##
- If something is unclear or not specified here, add it
- If you have to choose a convention, document it here
- On exceptional occasions, you can ignore these rules, for instance have a
  line of 82 characters does not really matter.

    The code is more what you'd call "guidelines" than actual rules. Welcome aboard the Black Pearl!
