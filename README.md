# ENS Système Digital - Group Project #

All documentation for the project will be linked here

- [Style Guide](style_guide.md)
- [Specifications](specifications.md)

- [Computer Organization and Design](https://books.google.fr/books?id=H7wxDQAAQBAJ&printsec=frontcover&redir_esc=y#v=onepage&q&f=false)
- [RISC V Instruction Set Manual] (https://riscv.org/wp-content/plugins/pdf-viewer/stable/web/viewer.html?file=https://content.riscv.org/wp-content/uploads/2017/05/riscv-spec-v2.2.pdf)
- [MIPS diagram reference] (http://ac.aua.am/Arm/Public/2017-Spring-Computer-Organization/Textbooks/ComputerOrganizationAndDesign5thEdition2014.pdf#page=288&zoom=page-width,22,621)

An interactive git branching tutorial :
- [LearnGitBranching.js](https://learngitbranching.js.org/)
